const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CarSchema = new Schema({
  brand: String,
  model: String,
  fuel: String,
  doors: Number,
  seats: Number,
  aircond: Boolean,
  nav: Boolean,
  type: String,
  gearbox: String,
  city: String,
  image: String,
});

module.exports = mongoose.model('car', CarSchema);