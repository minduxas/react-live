const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const moment = require('moment');

const app = express();

const port = process.env.PORT || 3000;

const databaseUser = 'react-live-admin';
const databasePassword = 'admin123';
const databaseName = 'react-live';
const databasePort = 13645;

moment.locale('lt');

const carsRoutes = require('./routes/cars');

app.use(cors());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

// database config
mongoose.connect(
  `mongodb://${databaseUser}:${databasePassword}@ds213645.mlab.com:${databasePort}/${databaseName}`,
  { useNewUrlParser: true }
);

app.get('/', (req, res) => res.send('CARZZZ !!!'));
app.use('/cars', carsRoutes);

app.listen(port, () =>
  console.log(`Car API application listening on port ${port}!`)
);


