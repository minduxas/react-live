const express = require('express');
const router = express.Router();
const moment = require('moment');
const Car = require('../models/car');

router.get('/', (req, res) => {
  Car.find({}).exec((error, cars) => {
    if (error) {
      res.status(500).send(error.message);
    } else {
      console.log(cars);
      res.send(cars.map(car => mapToCar(car)));
    }
  });
});

router.get('/:id', (req, res) => {
  Car.findOne({ _id: req.params.id }).exec((error, car) => {
    if (error) {
      res.status(500).send(error.message);
    } else {
      res.send(mapToCar(car));
    }
  });
});

router.post('/', (req, res) => {
  console.log(req.body);
  const newCar = new Car();

  newCar.brand = req.body.brand;
  newCar.model = req.body.model;
  newCar.fuel = req.body.fuel;
  newCar.doors = req.body.doors;
  newCar.seats = req.body.seats;
  newCar.aircond = req.body.aircond;
  newCar.nav = req.body.nav;
  newCar.type = req.body.type;
  newCar.gearbox = req.body.gearbox;
  newCar.city = req.body.city;
  newCar.image = req.body.image;

  newCar.save((error, car) => {
    if (error) {
      res.status(500).send(error.message);
    } else {
      console.log(car);
      res.status(201).send(car);
    }
  });
});

const mapToCar = car => {
  return {
    ID: car.id,
    Gamintojas: car.brand,
    Modelis: car.model,
    Degalai: car.fuel,
    Durys: car.doors,
    Vietos: car.seats,
    Kondicionierius: car.aircond,
    Navigacija: car.nav,
    Tipas: car.type,
    Pavaru_dežė: car.gearbox,
    Miestas: car.city,
    Nuotrauka: car.image,
  }
};

module.exports = router;